// resource "aws_s3_bucket" "Terraform_state" {
//   bucket = lower(join("-", tolist(["state", var.marca, var.environment])))
// }

resource "aws_s3_bucket" "mybuckets" {
  for_each = var.bucketnames
  bucket   = each.key
}
