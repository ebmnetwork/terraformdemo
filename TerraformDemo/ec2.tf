
#Instancia para el ambiente

resource "aws_instance" "ec2instance" {
  depends_on                           = [module.VPCouts.publicsgrid, module.VPCouts.subnetsid]
  ami                                  = "ami-047a51fa27710816e"
  instance_type                        = "t3.micro"
  // key_name                             = "demoterra2021"
  vpc_security_group_ids               = [module.VPCouts.publicsgrid]
  subnet_id                            = module.VPCouts.subnetsid[0]
  private_ip                           = module.VPCouts.PrivIPInstance
  instance_initiated_shutdown_behavior = "stop"
  lifecycle {
    prevent_destroy = false
  }
  user_data = <<-EOF
    #!/bin/bash
    yum update -y
    yum install -y httpd.x86_64
    systemctl start httpd.service
    systemctl enable httpd.service
    echo “Terraform demo from $(hostname -f)” > /var/www/html/index.html
	EOF

  tags = { Name = join("-", tolist(["Instance", var.marca, var.environment])) }
}

#Ip publica para la instancia
resource "aws_eip_association" "eipbastion_assoc" {
  depends_on    = [aws_instance.ec2instance, aws_eip.instanceeip]
  instance_id   = aws_instance.ec2instance.id
  allocation_id = aws_eip.instanceeip.id
}

resource "aws_eip" "instanceeip" {
  vpc  = true
  tags = { Name = join("-", tolist(["eIPInstance", var.marca, var.environment])) }
}

output "Instance" {
  value = aws_instance.ec2instance.public_dns
}