#definicion de variables y valores locales necesarios en el despliegue de la VPC y sus componentes
#En este modulo se construye la VPC y sus componentes

variable "cidrblock" {}

variable "marca" {}

variable "environment" {}

locals {
  publicblock01  = cidrsubnet(var.cidrblock, 8, 1)
  privateblock01 = cidrsubnet(var.cidrblock, 8, 3)
  PrivIPInstance = cidrhost(local.publicblock01, 10)
  subnets        = [local.publicblock01, local.privateblock01]

}

variable "subnames" {
  default = ["Public-Sub01", "Private-Sub01"]
}

variable "azones" {
  default = ["us-east-1c", "us-east-1c"]
}

