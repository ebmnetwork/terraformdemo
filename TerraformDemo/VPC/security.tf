
# Grupo de seguridad para subred publica
resource "aws_security_group" "publicsgr" {
  name        = "PublicSgr"
  description = "Public Security Group"
  vpc_id      = aws_vpc.vpc01.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    description = "Public Access"
  }
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    description = "Public Access"
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = { Name = join("-", tolist(["Sgr", "Public", var.marca, var.environment])) }
}