
module "VPCouts" {
  source      = "./VPC"
  marca       = var.marca
  environment = var.environment
  cidrblock   = var.cidrblock
}
