//Ingresar la region sobre la cual se va a trabajar
variable "aws_region" {
  default = "us-east-1"
}
//Confirmar la ruta del archivo de credenciales. Por defecto se encuentra en ~/.aws/credentials
variable "CRTfile" {
  default = "~/.aws/credentials"
}
//Ingresar perfil de AWS CLI con el cual se va a trabajar
variable "profile" {
  default = "default"
}

//Ingresar la marca propietaria del proyecto
//Use inicial en mayúscula
variable "marca" {
  default = "Pragma"
}
//Use DEV/QA/PDN para indicar que tipo de ambiente desplegará
variable "environment" {
  default = "DEMO"
}

//Indique el cidrblock para la VPC, validar que el segmento seleccionado no se use en otra cuenta
//ejemplo default = "10.88.0.0/16"  se recomienta usar segmentos con mascara /16
variable "cidrblock" {
  default = "10.93.0.0/16"
}

locals {
  tags = {
    marca       = var.marca
    environment = var.environment
  }
}

variable "bucketnames" {
  type    = set(string)
  default = ["kappa01demo", "kappa02demo", "kappa03demo"]
}

