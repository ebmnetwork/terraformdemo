#Datos de conexion a la cuenta de AWS
provider "aws" {
  region                  = var.aws_region
  shared_credentials_file = var.CRTfile
  profile                 = var.profile
  default_tags {
    tags = local.tags
  }
}