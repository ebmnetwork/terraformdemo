# Demo Webinar Terraform

**Terraform** Herramienta para construir, cambiar y crear versiones de infraestructura de manera segura y eficiente.

- Terraform interpreta los archivos de configuración en HashiCorp Configuration Language (HCL)
- Genera un plan de ejecución que describe lo que hará para alcanzar el estado deseado 
- Ejecuta para construir la infraestructura descrita. A medida que cambia la configuración, Terraform puede determinar qué cambió y crear planes de ejecución incrementales.

### Diagrama del Despliegue

![Alt text](misc/demo.png?raw=true "Demo Terraform")

El despliegue comprende los siguientes componentes:

- 1 VPC
     - 2 Subnets
     - 1 Route Table
     - 1 Internet Gateway
     - 1 Elastic IP
     - 1 EC2 Instance
     - 1 Security Group

- 3 Buckets 

## Requerimientos

Para el despliegue de este ambiente es mandatorio cumplir con los siguientes requisitos:


### Software
 - Terraform: v0.15.4
 - AWS CLI

## Despliegue Ambiente

Clonar el repositorio
```
git clone 
```

Terraform Init
```
terraform init
```

Terraform Plan
```
terraform plan
```

Terraform Apply
```
terraform apply
```

## Destruir Ambiente

Se puede destruir el ambiente haciendo uso del siguiente comando. 

```
terraform destroy
```

## Enlaces de Referencia

Terraform Installation: [Terraform Installation.](https://learn.hashicorp.com/tutorials/terraform/install-cli?in=terraform/aws-get-started)

Terraform Registry: [Terraform Registry.](https://registry.terraform.io/)

Learn Terraform: [Learn Terraform.](https://learn.hashicorp.com/terraform)
